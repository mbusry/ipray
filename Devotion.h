//
//  Devotion.h
//  testApp
//
//  Created by Michael on 5/11/14.
//  Copyright (c) 2014 Michael Usry. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Devotion : NSObject

@property(nonatomic) NSString *day;
@property(nonatomic) NSString *devotion;
@property(nonatomic) NSString *scripture;
@property(nonatomic) NSString *scriptureVersion;
@property(nonatomic) NSString *userInput;


@end
