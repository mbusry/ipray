//
//  UserSavedDevotion.m
//  testApp
//
//  Created by Michael on 5/21/14.
//  Copyright (c) 2014 Michael Usry. All rights reserved.
//

#import "UserSavedDevotion.h"

@implementation UserSavedDevotion

#define kCurrentDate      @"CurrentDate"
#define kWeatherTemp      @"WeatherTemp"
#define kCurrentDevotion  @"CurrentDevotion"
#define kUserDevotion     @"UserDevotion"
#define kLongitude        @"Longitude"
#define kLatitude         @"Latitude"
#define kUserDictionary   @"UserDictionary"

@synthesize loadedCurrentDate,loadedCurrentDevotion,loadedLatitude,loadedLongitude,loadedScripture,loadedScriptureVersion,loadedWeatherTemp,loadedUserDevotion,loadedUserDictionary;




- (void) encodeWithCoder:(NSCoder *)encoder {//encoder for the @property objects
    [encoder encodeObject:loadedCurrentDate forKey:kCurrentDate];
    [encoder encodeObject:loadedWeatherTemp forKey:kWeatherTemp];
    [encoder encodeObject:loadedCurrentDevotion forKey:kCurrentDevotion];
    [encoder encodeObject:loadedUserDevotion forKey:kUserDevotion];
    [encoder encodeObject:loadedLongitude forKey:kLongitude];
    [encoder encodeObject:loadedLatitude forKey:kLatitude];
    [encoder encodeObject:loadedUserDictionary forKey:kUserDictionary];
    
}

- (id)initWithCoder:(NSCoder *)decoder {//decode for the @property objects
    self = [self init];
    self.loadedCurrentDate = [decoder decodeObjectForKey:kCurrentDate];
    self.loadedWeatherTemp = [decoder decodeObjectForKey:kWeatherTemp];
    self.loadedCurrentDevotion = [decoder decodeObjectForKey:kCurrentDevotion];
    self.loadedUserDevotion = [decoder decodeObjectForKey:kUserDevotion];
    self.loadedLongitude = [decoder decodeObjectForKey:kLongitude];
    self.loadedLatitude = [decoder decodeObjectForKey:kLatitude];
    
        //always use a copy when decoding a dictionary
    NSDictionary *dict = [decoder decodeObjectForKey:kUserDictionary];
    self.loadedUserDictionary = [[NSMutableDictionary alloc] initWithDictionary:dict copyItems:YES];
    
    
    return self;
    
}

@end
