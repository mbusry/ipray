//
//  ViewController.m
//  testApp
//
//  Created by Michael on 5/7/14.
//  Copyright (c) 2014 Michael Usry. All rights reserved.
//

#import "ViewController.h"
#import "Devotion.h"
#import "UserSavedDevotion.h"

    //file name for saves
#define kFilename @"ipray.plist"
    //BG queue
#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
CGFloat screenWidth;




@interface ViewController ()




@end

@implementation ViewController

{
    CLLocationManager *locationManager;

    
}

@synthesize btnDone,inputAccView,weather;



-(NSString *)dataFilePath{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSLog(@"documentDirectory:%@",documentsDirectory);
    
    return [documentsDirectory stringByAppendingPathComponent:kFilename];
    
    
}


NSMutableArray *devotions;


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    devotions = [[NSMutableArray alloc]init];
    selectedImg = [[UIImage alloc]init];
    currentDate = [[NSDate alloc]init];
    userDevotion = [[NSString alloc]init];

    
        //
    [self createInputAccessoryView];
    [userInputTextView setInputAccessoryView:inputAccView];

    
  
        //placeholder text for textView (userInputTextView)
    userInputTextView.text = @"Touch to add your thoughts";
    userInputTextView.textColor = [UIColor lightGrayColor];
    userInputTextView.delegate=self;
    
        //setup for file path
    
    NSString *filePath = [self dataFilePath];
    

        //what to do if the file exist
    if ([[NSFileManager defaultManager]fileExistsAtPath:filePath])
    {
            //import file into savedDevotion when save is done write ipray.plist back out
        NSLog(@"the file exist!");
        
        
            // Build the array from the ipray.plist
//        loadedFile = [[NSMutableArray alloc] initWithContentsOfFile:filePath];
        
            //read the ipray.plist into mutableDictionary
        loadedDictionaryFile = [[NSMutableDictionary alloc]init];
//        loadedDictionaryFile = [NSMutableDictionary dictionaryWithContentsOfFile:[self dataFilePath]];
        
        
            // Show the string values
//        for (NSDictionary *str in loadedDictionaryFile)
//            NSLog(@"--%@", str);
        

            //set the loadedFile to the UserSavedDevotion object
//        for (int i=0; loadedFile.count; i++) {
                //set data to object
                //loadedDay = loadedFile(i)
                //....
        
            //unarchive ipray.plist
        loadedDictionaryFile  = [NSKeyedUnarchiver unarchiveObjectWithFile:[self dataFilePath]];
        NSLog(@"usd:%@",loadedDictionaryFile);
        
            //devotionLabel.text = ((Devotion *) [devotions objectAtIndex:i]).devotion;
        
                             
        

    }
    /*
        //dictionary loaded
    NSDictionary *dictFromFile = [NSDictionary dictionaryWithContentsOfFile:filePath];
    for (NSString *key in dictFromFile)
        NSLog(@"key:%@ value:%@",key,[dictFromFile valueForKey:@"Key"]);
*/
        // Gives the current day
    cd = [[NSDateFormatter alloc] init];
    [cd setDateFormat: @"EEEE"];
    
    NSLog(@"the day is: %@", [cd stringFromDate:[NSDate date]]);
    currentDay = [cd stringFromDate:[NSDate date]];
    
    NSLog(@"the day is: %@", currentDay);

        //setup Devotion object for each day (currently day of the week only)
    Devotion *sunday = [[Devotion alloc]init];
    [sunday setDay:@"Sunday"];
    [sunday setDevotion:@"Peace I leave with you, My peace I give to you; not as the world gives do I give to you.  Let not your heart be trouble, neither let it be afraid."];
    [sunday setScripture:@"John 14:27"];
    [sunday setScriptureVersion:@"NKJV"];
    [devotions addObject:sunday];
    
    Devotion *monday = [[Devotion alloc]init];
    [monday setDay:@"Monday"];
    [monday setDevotion:@"Seek the Lord and His strength; Seek His face evermore!"];
    [monday setScripture:@"1 Chronicles 16:11"];
    [monday setScriptureVersion:@"NKJV"];
    [devotions addObject:monday];

    Devotion *tuesday = [[Devotion alloc]init];
    [tuesday setDay:@"Tuesday"];
    [tuesday setDevotion:@"Confess your trespasses to one another, and pray for one another, that you may be healed. The effective, fervent prayer of a righteous man avails much."];
    [tuesday setScripture:@"James 5:16"];
    [tuesday setScriptureVersion:@"NKJV"];
    [devotions addObject:tuesday];

    Devotion *wednesday = [[Devotion alloc]init];
    [wednesday setDay:@"Wednesday"];
    [wednesday setDevotion:@"Be anxious for nothing, but in everything by prayer and supplication, with thanksgiving, let your requests be made known to God."];
    [wednesday setScripture:@"Philippians 4:6"];
    [wednesday setScriptureVersion:@"NKJV"];
    [devotions addObject:wednesday];

    Devotion *thursday = [[Devotion alloc]init];
    [thursday setDay:@"Thursday"];
    [thursday setDevotion:@"The Lord is near to all who call upon Him, To all who call upon Him in truth."];
    [thursday setScripture:@"Psalm 145:18"];
    [thursday setScriptureVersion:@"NKJV"];
    [devotions addObject:thursday];

    Devotion *friday = [[Devotion alloc]init];
    [friday setDay:@"Friday"];
    [friday setDevotion:@"Likewise the Spirit also helps in our weaknesses. For we do not know what we should pray for as we ought, but the Spirit Himself makes intercession for us with groanings which cannot be uttered."];
    [friday setScripture:@"Romans 8:26"];
    [friday setScriptureVersion:@"NKJV"];
    [devotions addObject:friday];

    Devotion *saturday = [[Devotion alloc]init];
    [saturday setDevotion:@"pray without ceasing"];
    [saturday setScripture:@"1 Thessalonians 5:17"];
    [saturday setScriptureVersion:@"NKJV"];
    [devotions addObject:saturday];
    
    

        //loop for this day's devotion
    for (int i=0; i<[devotions count]; i++)
    {
            //
                   NSLog(@"i=%d",i);
//        NSLog(@"day=%@",(((Devotion *) [devotions objectAtIndex:i]).day));
            //
        NSString *d = ((((Devotion *) [devotions objectAtIndex:i]).day));
        
            //comparing two strings for devotionDay = currentDay
        if ([d isEqualToString:currentDay])
        {
            
            
                //set devotion text from object
            devotionLabel.text = ((Devotion *) [devotions objectAtIndex:i]).devotion;
            
                //set scripture text from object
            scriptureLabel.text = ((Devotion *) [devotions objectAtIndex:i]).scripture;
            
                //set scripture version from object
            versionLabel.text = ((Devotion *) [devotions objectAtIndex:i]).scriptureVersion;
            
                //dateLabel for today's date
            NSDateFormatter *dateFormatter= [[NSDateFormatter alloc] init];

            [dateFormatter setDateStyle:(NSDateFormatterShortStyle)];
            NSString* todayDate = [dateFormatter stringFromDate:currentDate];
            
            dateLabel.text = todayDate;
            currentDevotion = devotionLabel.text;
            
            
//            NSLog(@"currentDevotion:%@",currentDevotion);
            
            
            break;
        }
    }
    
    
        //get location for current weather
    locationManager = [[CLLocationManager alloc]init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    [locationManager startUpdatingLocation];
    
    
    
        //end of json weather info

   
    
}


- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"I Could Not Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    latitude = @"NA";
    longitude = @"NA";

    
    [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil) {
            //format latitude and longitude strings
        longitude = [NSString stringWithFormat:@"%.18f",currentLocation.coordinate.longitude];
        latitude = [NSString stringWithFormat:@"%.18f",currentLocation.coordinate.latitude];
        
//        NSLog(@"Long:%@",longitude);
//        NSLog(@"Lat :%@",latitude);
     /*
            weather json link
            key=5qwnnspwyfufndc3ffxmy25t
        
            Main example:
                NSURL *weatherURL = [NSURL URLWithString:@"http://api.worldweatheronline.com/free/v1/weather.ashx?key=5qwnnspwyfufndc3ffxmy25t&q='LAT','LONG'&fx=no&format=json"];

            strings to put the full URL together for the weather json
        NSString *weatherURL1 = [NSString stringWithFormat:@"http://api.worldweatheronline.com/free/v1/weather.ashx?key=5qwnnspwyfufndc3ffxmy25t&q="];
     */
        NSURL *weatherURL1 = [NSURL URLWithString:@"http://api.worldweatheronline.com/free/v1/weather.ashx?key=5qwnnspwyfufndc3ffxmy25t&q="];

        
        NSURL *weatherURL2 = [NSURL URLWithString:@"&fx=no&format=json"];
        
        NSString *weatherString = [NSString stringWithFormat:@"%@%@,%@%@",weatherURL1,latitude,longitude,weatherURL2];
        
            //URL for weather object with current latitude and longitude
        NSURL *weatherURLFull = [NSURL URLWithString:weatherString];
        
        NSLog(@"weatherURL:%@",weatherURLFull);
        
        dispatch_async(kBgQueue, ^{
            NSData* data = [NSData dataWithContentsOfURL:
                            weatherURLFull];
            [self performSelectorOnMainThread:@selector(getJSON:)
                                   withObject:data waitUntilDone:YES];
        });
 
    
    }
        //used to stop location services so I get the info once and not constantly
    [manager stopUpdatingLocation];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)save:(id)sender {
    if (mDict == nil) {
    
        mDict = [NSMutableDictionary dictionary];

    }
        //input from user
    NSString *userInput = userInputTextView.text;
    NSString *scr = scriptureLabel.text;
    NSString *s = versionLabel.text;
   
        //camera picture taken
//    UIImage *savedPicture = selectedImg;
    
//    NSLog(@"userInput %@",userInput);
    
    NSMutableArray *devotionSaved = [[NSMutableArray alloc]init];
    [devotionSaved addObject:currentDate];
    if (currentWeatherTemp == nil)
    {
        currentWeatherTemp = @"NA";
    }
    [devotionSaved addObject:currentWeatherTemp];
    [devotionSaved addObject:longitude];
    [devotionSaved addObject:latitude];
    [devotionSaved addObject:currentDevotion];
    if ([userInput isEqualToString:@"Touch to add your thoughts"])
    {
        userInput = @"Nothing added";
    }

    [devotionSaved addObject:userInput];
    [devotionSaved addObject:scr];
    [devotionSaved addObject:s];


/*
    for (NSString *str in loadedFile)
        NSLog(@"---%@", str);
    
    for (NSString *st in devotionSaved)
        NSLog(@"+++%@", st);

*/
        //create dictionary
    NSDictionary *devotionSavedDictionary = @{currentDate : devotionSaved};
        //create mutabledictionary to add devotionSavedDictionary
    [mDict addEntriesFromDictionary:devotionSavedDictionary];
    [mDict addEntriesFromDictionary:loadedDictionaryFile];
                                                
    NSLog(@"Dictionary: %@",devotionSavedDictionary);
    NSLog(@"MutableDictionary: %@",mDict);

        //write  archived MutableDictionary
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:mDict];
//
    [data writeToFile:[self dataFilePath] atomically:YES];
    
    
        //save devotionSaved to file
//    [devotionSaved writeToFile:[self dataFilePath] atomically:YES];
//    [mDict writeToFile:[self dataFilePath] atomically:YES];


    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Save" message:@"Completed" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    
    if (alertView != nil)
    {
        [alertView show];
        devotionSavedDictionary = nil;
        devotionSaved = nil;
    }

}

- (void)getJSON:(NSData *)responseData {
        //parse out the json data
    NSError* error;
    NSDictionary* json = [NSJSONSerialization
                          JSONObjectWithData:responseData //1
                          
                          options:kNilOptions
                          error:&error];
    weather = [[NSDictionary alloc]init];
    
    weather = [json objectForKey:@"current_condition"];
    
    
    NSDictionary *data = [json valueForKey:@"data"];
    NSDictionary *dat = [data valueForKey:@"current_condition"];
    NSDictionary *cc = [dat valueForKey:@"temp_F"];
    
    

//    NSLog(@"current Temp: %@",cc);
    

        //string stripper to get only numbers from the NSDictionary for cc (current_condition)
    NSString *originalString = [NSString stringWithFormat:@"%@",cc];

    NSMutableString *strippedString = [NSMutableString
                                       stringWithCapacity:originalString.length];
    
    NSScanner *scanner = [NSScanner scannerWithString:originalString];
    NSCharacterSet *numbers = [NSCharacterSet
                               characterSetWithCharactersInString:@"0123456789"];
    
    while ([scanner isAtEnd] == NO) {
        NSString *buffer;
        if ([scanner scanCharactersFromSet:numbers intoString:&buffer]) {
            [strippedString appendString:buffer];
            
        } else {
            [scanner setScanLocation:([scanner scanLocation] + 1)];
        }
    }
    
    NSLog(@"%@", strippedString);
        // "123123123"
    
    tempLabel.text = [NSString stringWithFormat:@"%@",strippedString];
    currentWeatherTemp = strippedString;



}


- (IBAction)photo:(id)sender {
    NSLog(@"photo");

    UIImagePickerController *pickerController = [[UIImagePickerController alloc]init];
    if (pickerController != nil) {
            //the entire library
            //        pickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
            //the saved library
        pickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        
        
        
            // delegate pointing back to here
        pickerController.delegate = self;
        
            //Do I allow editing?
        pickerController.allowsEditing = true;
        
            //display the pickerController on this screen
        [self presentViewController:pickerController animated:true completion:nil];
        
    }

    
}

- (IBAction)camera:(id)sender {
    NSLog(@"camera");
    UIImagePickerController *pickerController = [[UIImagePickerController alloc]init];
    
    if (pickerController !=nil)
    {
        pickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        pickerController.delegate = self;
        
        pickerController.allowsEditing = true;
        
        pickerController.videoQuality = UIImagePickerControllerQualityTypeMedium;
        
        pickerController.mediaTypes = [NSArray arrayWithObjects:(NSString*) kUTTypeMovie,nil];
        
        [self presentViewController:pickerController animated:true completion:nil];
    }

}

- (IBAction)dismiss:(id)sender
{
        // dismiss the KB
    
    NSLog(@"dismiss KB");
    [self.view endEditing:YES];
    
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info;
{
        NSLog(@"%@",[info description]);
    
        //movie selected
    if ([info valueForKey:UIImagePickerControllerMediaURL]) {
            //path for the movie file
        NSURL *urlString = [info valueForKey:UIImagePickerControllerMediaURL];
        
        if (urlString != nil)
        {
                //convert the url of the file to the path
            videoPath = [urlString path];
            NSLog(@"videoPath:%@",videoPath);
            
            
            
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Save the Movie?" message:nil delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes",nil];
            alert.alertViewStyle = UIAlertViewStyleDefault;
            
            [alert show];
            
            
        }
    }else {
            //camera picture
        
        NSLog(@"Camera Info Description:\n%@",[info description]);
        
        selectedImg = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
            NSLog(@"selectedImage:%@",selectedImg);
        if (selectedImg !=nil) {
                //I have an image
            
        }
        
        
        
        [picker dismissViewControllerAnimated:YES completion:nil];
//        [self performSegueWithIdentifier:@"imageView" sender:nil];
    }
    
}


-(void)image:(UIImage *)image didFinishSavingWithError: (NSError *)error contextInfo: (void *) contextInfo
{
        //on error do this saving the image
    if (error !=nil) {
            //NSLog(@"%@",[error description]);
            //NSLog(@"didFinishSavingWithError: %@",[error description]);
        UIAlertView *alert = [[UIAlertView alloc]init];
        alert = [[UIAlertView alloc] initWithTitle:@"ERROR" message:@"Please check log" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        
        [alert show];
        
    }else {
            //NSLog(@"save was successful");
        [self dismissViewControllerAnimated:true completion:nil];
        
    }
}

- (void)video:(NSString *)videoPath didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
        //on error do this saving the video
    if (error !=nil) {
            //NSLog(@"didFinishSavingWithError: %@",[error description]);
        UIAlertView *alert = [[UIAlertView alloc]init];
        alert = [[UIAlertView alloc] initWithTitle:@"ERROR" message:@"Please check log" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        
        [alert show];
        [self dismissViewControllerAnimated:true completion:nil];
        
        
    }
    else {
            //NSLog(@"save was successful");
        
        [self dismissViewControllerAnimated:true completion:nil];
        
    }
}


- (void)textViewDidBeginEditing:(UITextView *)textView;
{
        //remove placeholder text
    userInputTextView.text = @"";
    userInputTextView.textColor = [UIColor blackColor];

    
        //move fields up when keyboard is active
    [UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDelegate:self];
	[UIView setAnimationDuration:0.5];
	[UIView setAnimationBeginsFromCurrentState:YES];
	userInputTextView.frame = CGRectMake(userInputTextView.frame.origin.x, (userInputTextView.frame.origin.y - 100.0), userInputTextView.frame.size.width, userInputTextView.frame.size.height);
    dateLabel.frame = CGRectMake(dateLabel.frame.origin.x, (dateLabel.frame.origin.y - 100.0), dateLabel.frame.size.width, dateLabel.frame.size.height);
    tempLabel.frame = CGRectMake(tempLabel.frame.origin.x, (tempLabel.frame.origin.y - 100.0), tempLabel.frame.size.width, tempLabel.frame.size.height);
	devotionLabel.frame = CGRectMake(devotionLabel.frame.origin.x, (devotionLabel.frame.origin.y - 100.0), devotionLabel.frame.size.width, devotionLabel.frame.size.height);
	scriptureLabel.frame = CGRectMake(scriptureLabel.frame.origin.x, (scriptureLabel.frame.origin.y - 100.0), scriptureLabel.frame.size.width, scriptureLabel.frame.size.height);
	versionLabel.frame = CGRectMake(versionLabel.frame.origin.x, (versionLabel.frame.origin.y - 100.0), versionLabel.frame.size.width, versionLabel.frame.size.height);
    dateNameLabel.frame = CGRectMake(dateNameLabel.frame.origin.x, (dateNameLabel.frame.origin.y - 100.0), dateNameLabel.frame.size.width, dateNameLabel.frame.size.height);
    weatherNameLabel.frame = CGRectMake(weatherNameLabel.frame.origin.x, (weatherNameLabel.frame.origin.y - 100.0), weatherNameLabel.frame.size.width, weatherNameLabel.frame.size.height);
	titleLabel.frame = CGRectMake(titleLabel.frame.origin.x, (titleLabel.frame.origin.y - 100.0), titleLabel.frame.size.width, titleLabel.frame.size.height);
    weatherDegreeLabel.frame = CGRectMake(weatherDegreeLabel.frame.origin.x, (weatherDegreeLabel.frame.origin.y - 100.0), weatherDegreeLabel.frame.size.width, weatherDegreeLabel.frame.size.height);

        // Call the createInputAccessoryView method we created earlier.
        // By doing that we will prepare the inputAccView.
    [self createInputAccessoryView];
    
        // Now add the view as an input accessory view to the selected textfield.
    [userInputTextView setInputAccessoryView:inputAccView];
    


	[UIView commitAnimations];
}
- (void)textViewDidEndEditing:(UITextView *)textView;

{
    [UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDelegate:self];
	[UIView setAnimationDuration:0.5];
	[UIView setAnimationBeginsFromCurrentState:YES];
	userInputTextView.frame = CGRectMake(userInputTextView.frame.origin.x, (userInputTextView.frame.origin.y + 100.0), userInputTextView.frame.size.width, userInputTextView.frame.size.height);
    dateLabel.frame = CGRectMake(dateLabel.frame.origin.x, (dateLabel.frame.origin.y + 100.0), dateLabel.frame.size.width, dateLabel.frame.size.height);
    tempLabel.frame = CGRectMake(tempLabel.frame.origin.x, (tempLabel.frame.origin.y + 100.0), tempLabel.frame.size.width, tempLabel.frame.size.height);
	devotionLabel.frame = CGRectMake(devotionLabel.frame.origin.x, (devotionLabel.frame.origin.y + 100.0), devotionLabel.frame.size.width, devotionLabel.frame.size.height);
	scriptureLabel.frame = CGRectMake(scriptureLabel.frame.origin.x, (scriptureLabel.frame.origin.y + 100.0), scriptureLabel.frame.size.width, scriptureLabel.frame.size.height);
	versionLabel.frame = CGRectMake(versionLabel.frame.origin.x, (versionLabel.frame.origin.y + 100.0), versionLabel.frame.size.width, versionLabel.frame.size.height);
    dateNameLabel.frame = CGRectMake(dateNameLabel.frame.origin.x, (dateNameLabel.frame.origin.y + 100.0), dateNameLabel.frame.size.width, versionLabel.frame.size.height);
	weatherNameLabel.frame = CGRectMake(weatherNameLabel.frame.origin.x, (weatherNameLabel.frame.origin.y + 100.0), weatherNameLabel.frame.size.width, weatherNameLabel.frame.size.height);
	titleLabel.frame = CGRectMake(titleLabel.frame.origin.x, (titleLabel.frame.origin.y + 100.0), titleLabel.frame.size.width, titleLabel.frame.size.height);
    weatherDegreeLabel.frame = CGRectMake(weatherDegreeLabel.frame.origin.x, (weatherDegreeLabel.frame.origin.y + 100.0), weatherDegreeLabel.frame.size.width, weatherDegreeLabel.frame.size.height);



	[UIView commitAnimations];

    userInputTextView.textColor = [UIColor lightGrayColor];

}

-(void)createInputAccessoryView{
        // Extra view for the done button to dismiss the keyboard
    
    inputAccView = [[UIView alloc] initWithFrame:CGRectMake(0, 0.0, screenWidth, 25)];
    
    [inputAccView setBackgroundColor:[UIColor darkGrayColor]];
    
    [inputAccView setAlpha: 0.6];
    
    
    btnDone = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnDone setFrame:CGRectMake(240.0, 0.0f, 80.0f, 25)];
    [btnDone setTitle:@"Done" forState:UIControlStateNormal];
    [btnDone setBackgroundColor:[UIColor lightTextColor]];
    [btnDone setTitleColor:[UIColor darkTextColor] forState:UIControlStateNormal];
    [btnDone setAlpha:0.7];
    [btnDone addTarget:self action:@selector(dismiss:) forControlEvents:UIControlEventTouchUpInside];
    
        // Now that our buttons are ready we just have to add them to our view.
    [inputAccView addSubview:btnDone];
}

- (void)getScreenSize {
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    screenWidth = screenRect.size.width;
}



@end
