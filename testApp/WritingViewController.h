//
//  WritingViewController.h
//  testApp
//
//  Created by Michael on 5/14/14.
//  Copyright (c) 2014 Michael Usry. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Devotion.h"

@interface WritingViewController : UIViewController

- (IBAction)clearButton:(id)sender;
- (IBAction)saveButton:(id)sender;
- (IBAction)doneButton:(id)sender;


@end
