//
//  UserSavedDevotion.h
//  testApp
//
//  Created by Michael on 5/21/14.
//  Copyright (c) 2014 Michael Usry. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserSavedDevotion : NSObject <NSCoding>

@property(nonatomic) NSString *loadedCurrentDate;
@property(nonatomic) NSString *loadedCurrentDevotion;
@property(nonatomic) NSString *loadedScripture;
@property(nonatomic) NSString *loadedScriptureVersion;
@property(nonatomic) NSString *loadedLongitude;
@property(nonatomic) NSString *loadedLatitude;
@property(nonatomic) NSString *loadedWeatherTemp;
@property(nonatomic) NSString *loadedUserDevotion;
@property(nonatomic) NSDictionary *loadedUserDictionary;


-(void)encodeWithCoder:(NSCoder *)encoder;

-(id)initWithCoder:(NSCoder *)decoder;




@end
