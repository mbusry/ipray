//
//  PrayerListTableViewController.h
//  testApp
//
//  Created by Michael on 5/11/14.
//  Copyright (c) 2014 Michael Usry. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Devotion.h"
#import "UserSavedDevotion.h"


@interface PrayerListTableViewController : UITableViewController
{
    NSMutableDictionary *loadedDictionaryFile;
    NSArray *keyArray;
    NSDateFormatter *cd;
    NSString *currentDay;

}

@end
