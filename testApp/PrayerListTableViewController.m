//
//  PrayerListTableViewController.m
//  testApp
//
//  Created by Michael on 5/11/14.
//  Copyright (c) 2014 Michael Usry. All rights reserved.
//

#import "PrayerListTableViewController.h"
#import "TableDetailsViewController.h"

    //file name for saves
#define kFilename @"ipray.plist"
    //BG queue
#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)



@interface PrayerListTableViewController ()

@end

@implementation PrayerListTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    //read the ipray.plist into mutableDictionary
    loadedDictionaryFile = [[NSMutableDictionary alloc]init];

    loadedDictionaryFile  = [NSKeyedUnarchiver unarchiveObjectWithFile:[self dataFilePath]];
    NSLog(@"usd:%@",loadedDictionaryFile);
    
    keyArray = [loadedDictionaryFile allKeys];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSString *)dataFilePath{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSLog(@"documentDirectory:%@",documentsDirectory);
    
    return [documentsDirectory stringByAppendingPathComponent:kFilename];
    
    
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
//#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
        //#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return [keyArray count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    // Configure the cell...
    
    NSString *key = [keyArray objectAtIndex:indexPath.row];
    NSArray *ar = [loadedDictionaryFile objectForKey:key];
    
    NSLog(@"key:%@",key);
    NSLog(@"date: %@",ar[0]);
    NSLog(@"temp: %@",ar[1]);
    NSLog(@"saved Devotion: %@",ar[4]);
    NSLog(@"saved Prayer: %@",ar[5]);
    
        //pass ar to display
    cd = [[NSDateFormatter alloc] init];
    [cd setDateFormat: @"h:mm a, MMMM dd yyyy"];
    
    NSLog(@"the day is: %@", [cd stringFromDate:[NSDate date]]);
    currentDay = [cd stringFromDate:ar[0]];

    UILabel *dateLabel = (UILabel *)[cell viewWithTag:0];
    dateLabel.text = currentDay;//convert to string


    
        //set values here
    
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
        //create a variable to know what cell was clicked

    NSIndexPath *indexPath = [self.tableView indexPathForCell:(UITableViewCell*)sender];
    
    
    TableDetailsViewController *dvc = [segue destinationViewController];
    NSString *key = [keyArray objectAtIndex:indexPath.row];
    NSArray *ar = [loadedDictionaryFile objectForKey:key];

    NSLog(@"\nkey at index: %@\n",key);

    NSLog(@"\nar at index: %@\n",ar);

    [dvc setPAR:ar];
}


@end
