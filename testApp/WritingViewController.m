//
//  WritingViewController.m
//  testApp
//
//  Created by Michael on 5/14/14.
//  Copyright (c) 2014 Michael Usry. All rights reserved.
//

#import "WritingViewController.h"

@interface WritingViewController ()

@end

@implementation WritingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)clearButton:(id)sender {
        //Clear writing
    
    
        //Alert for data cleared (not saved)
    UIAlertView *clearAlert = [[UIAlertView alloc]
                               initWithTitle:@"Clear?" message:@"Are you sure?" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [clearAlert show];
        //dismiss this controller and go home (ViewController)
    [self dismissViewControllerAnimated:YES completion:nil];


}

- (IBAction)saveButton:(id)sender {
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Save" message:@"Complete" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];

}

- (IBAction)doneButton:(id)sender {
}
@end
