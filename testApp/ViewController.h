//
//  ViewController.h
//  testApp
//
//  Created by Michael on 5/7/14.
//  Copyright (c) 2014 Michael Usry. All rights reserved.
//
/*
 
 Main UI will take the picture,movie, writing and save to savedDevotion
 
 savedDevotion: currentDate,currentWeatherIcon,devotion,picture(selImage),movie,writing,
 
 savedDevotion obj = each of these objects above.
 
 to get the day of the week:  
 

 
 
 
 */

#import <UIKit/UIKit.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <CoreLocation/CoreLocation.h>


@interface ViewController : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate, CLLocationManagerDelegate, UITextViewDelegate,UITextFieldDelegate>
{
    UIImage *selectedImg;
    NSString *videoPath;
    NSMutableArray *savedDevotion;
    NSMutableArray *loadedFile;
    NSDate *currentDate;
    NSString *currentWeatherTemp;
    NSDateFormatter *cd;
    NSString *currentDay;
    NSString *longitude;
    NSString *latitude;
    NSString *currentDevotion;
    NSString *userDevotion;
    UIView *inputAccView;
    NSDictionary *weather;
    NSDictionary *loadedDictionaryFile;
    NSMutableDictionary *mDict;
    

    
    __weak IBOutlet UILabel *weatherDegreeLabel;
    __weak IBOutlet UILabel *titleLabel;
    __weak IBOutlet UILabel *weatherNameLabel;
    __weak IBOutlet UILabel *dateNameLabel;
    __weak IBOutlet UITextView *userInputTextView;
    __weak IBOutlet UILabel *dateLabel;//current date
    __weak IBOutlet UILabel *tempLabel;//current temp **FIX**
    __weak IBOutlet UIImageView *weatherIconImage;//weather icon **FIX**
    __weak IBOutlet UILabel *devotionLabel;//devotion from input file
    __weak IBOutlet UILabel *scriptureLabel;//scripture input file
    __weak IBOutlet UILabel *versionLabel;//Bible translation from input file
    
}
@property (nonatomic, retain)UIButton *btnDone;
@property (nonatomic, retain)UIView *inputAccView;
@property (nonatomic, strong)NSDictionary *weather;


- (IBAction)save:(id)sender;
- (IBAction)photo:(id)sender;
- (IBAction)camera:(id)sender;
- (IBAction)dismiss:(id)sender;

@end
