
    //
//  TableDetailsViewController.m
//  testApp
//
//  Created by Michael on 5/13/14.
//  Copyright (c) 2014 Michael Usry. All rights reserved.
//

#import "TableDetailsViewController.h"
#import "PrayerListTableViewController.h"


@interface TableDetailsViewController ()


@end

@implementation TableDetailsViewController

@synthesize pAR,dateLabel,tempLabel,savedDevotionLabel,savedPrayerLabel,savedScripture,savedScriptureVersion;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    cd = [[NSDateFormatter alloc] init];
    [cd setDateStyle:(NSDateFormatterShortStyle)];
    
    NSLog(@"the day is: %@", [cd stringFromDate:[NSDate date]]);
    currentDay = [cd stringFromDate:pAR[0]];
    
    NSLog(@"currentDay:%@",currentDay);
    NSLog(@"0:%@",pAR[0]);
    NSLog(@"1:%@",pAR[1]);
    NSLog(@"4:%@",pAR[4]);
    NSLog(@"5:%@",pAR[5]);

    
    /*
     NSDateFormatter *dateFormatter= [[NSDateFormatter alloc] init];
     
     [dateFormatter setDateStyle:(NSDateFormatterShortStyle)];
     NSString* todayDate = [dateFormatter stringFromDate:currentDate];
     
     dateLabel.text = todayDate;

     */
    
    dateLabel.text = currentDay;
    tempLabel.text = pAR[1];
    savedDevotionLabel.text = pAR[4];
    savedPrayerLabel.text = pAR[5];
    savedScripture.text = pAR[6];
    savedScriptureVersion.text = pAR[7];


}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
