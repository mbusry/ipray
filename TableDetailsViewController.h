//
//  TableDetailsViewController.h
//  testApp
//
//  Created by Michael on 5/13/14.
//  Copyright (c) 2014 Michael Usry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableDetailsViewController : UIViewController

{
    
    NSArray *pAR;
    NSDateFormatter *cd;
    NSString *currentDay;
    NSDate *currentDate;
    
}

@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *tempLabel;
@property (weak, nonatomic) IBOutlet UILabel *savedDevotionLabel;
@property (weak, nonatomic) IBOutlet UILabel *savedPrayerLabel;
@property (weak, nonatomic) IBOutlet UILabel *savedScripture;
@property (weak, nonatomic) IBOutlet UILabel *savedScriptureVersion;


@property (nonatomic, strong) NSArray *pAR;

@end
